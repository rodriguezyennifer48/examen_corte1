package com.example.exa_corte1_java;

import android.widget.Toast;

public class CuentaBanco {
    // Declaración de variables de la clase
    private int numCuenta;
    private String nombre;
    private String banco;
    private float saldo;

    // Constructor
    public CuentaBanco(int numCuenta, String nombre, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }
    // Métodos de clase
    public void depositar(float dinero) {
        float saldoActualizado = 0;
        this.saldo = this.saldo + dinero;
    }

    public boolean retirar(float dinero) {
        boolean bandera = true;
        if (dinero > this.saldo) {
            bandera = false;
        }
        return bandera;
    }
}

