package com.example.exa_corte1_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    // Declaracion de las variables
    private EditText txtNumCuenta;
    private EditText txtNombre;
    private EditText txtNombreBanco;
    private EditText txtSaldo;

    private Button btnEnviar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }
    private void iniciarComponentes() {
        //Cajas de texto
        txtNumCuenta = (EditText) findViewById(R.id.txtNumeroCuenta);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtNombreBanco = (EditText) findViewById(R.id.txtNombreBanco);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);

        //botones
        btnSalir = (Button) findViewById(R.id.btnSalir);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
    }
    private void enviar() {
        String NumCuenta;
        String Nombre;
        String Banco;
        String Saldo;

        Banco = getApplicationContext().getResources().getString(R.string.strBanco);

        //Hacer los paquetes para enviar la informacion
        Bundle bundle = new Bundle();
        bundle.putString("Nombre",txtNombre.getText().toString());
        bundle.putString("Banco",txtNombreBanco.getText().toString());
        bundle.putString("Saldo",txtSaldo.getText().toString());

        //crear el intent para llamar a otra actividad
        Intent intent = new Intent(MainActivity.this, CuentaBancoActivity.class);
        intent.putExtras(bundle);

        //iniciar la actividad esperando o no respuesta
        startActivity(intent);
    }
    private void salir() {
        finish();
    }
}